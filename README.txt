Ludesse
Commands
To use one of the commands below use "systemcall:" before the command.

-	av username
-	av @username
This command shows the profile picture of username

-	ban @username
This command bans the member username

-	unban username#4444
Unbans username#4444

-	kick username
-	kick @username
Kicks the member username

-	invite
Sends a link to invite the bot in your server

-	support
Sends a link to ludesse's main support server and gives information on future projects concerning Ludesse's development

-	info
Shows information about the current server

-	member
Shows the number of people in the current server

-	purge 999
Deletes the last 999 messages that are inside the current text channel

-	changeRole @username "roleName"
Assigns a role "roleName" to username

-	removeRole @username "roleName"
@username will lose his former role

-	ping
Enables the bot to show it's latency value and also the server's latency

-	roleSupression roleName
Deletes the role "roleName"

-	search sentence
Searches up a sentence on the internet 

-	mute @username
Mutes the person "@username"
/!\ To use this command the bot MEE6 needs to be on your server, and the role muted needs to exist

-	unmute @username
Unmutes the person "@username"
/!\ To use this command the bot MEE6 needs to be on your server, and the role muted needs to exists




The next command is for music, so you need to follow the step below

-	join
In order for this command to work, you must already be in a voice channel 

-	queue https://www.youtube.com/musicName
Put in queue https://www.youtube.com/musicName
/!\ You can only used youtube url

-	play
Plays the current queue

-	pause
Pauses the current queue

-	resume
Resumes the current song that is in pause

-	view
Shows all the queue

-	stop
Disconnects the bot from the voice channel
