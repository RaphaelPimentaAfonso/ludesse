# bot named : Ludesse
# creator   : Darki#4661
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
import datetime
import os
import discord
import random
import youtube_dl
import asyncio
import logging
from random import choice
from discord import member
from subprocess import Popen, PIPE
from discord.ext import commands, tasks
from discord.utils import get
from discord.ext.commands import Bot
from discord.ext.commands import has_permissions, MissingPermissions
from googlesearch import search
import json

TOKEN = os.getenv('Nzk5MzI5ODU1NzYxMTU0MTI4.YAB_8A.EcLE8B8S79aq_e1g-SZduVVeM4w')


###############################################CONSTANTE###############################################
PERMISSIONS_DENIED='You do not have permission to do that'
ARGUMENT_MISSING='An argument is missing'
COMMAND_NOT_FOUND='Command not regognized'
NOT_IN_A_VOICE_CHANNEL='You are not in a voice channel'
LUCE_ID=800113219703537694
DARKI_ID=375564893992124416
PRIVATE="This command is private and can only be used per Darki"
channel_list=[]
logging.basicConfig(  
    filename='logs.log', 
    format='%(asctime)s %(message)s',
    level=logging.INFO,
    datefmt='%d/%m/%Y %H:%M:%S')

###############################################bot variable set###############################################
bot = discord.Client()
bot = commands.Bot(command_prefix=['Systemcall : ', 'Systemcall: ', 'Systemcall:', 'Systemcall :', 'systemcall : ', 'systemcall: ', 'systemcall:', 'systemcall :'])   
status = ["Systemcall:help", "Systemcall:support"]
queue = []
chatty = ChatBot('Ludesse',
              logic_adapters=[
                  'chatterbot.logic.BestMatch',
                  'chatterbot.logic.TimeLogicAdapter'
              ],
              srotage_adapter='chatterbot.storage.SQLStorageAdapter',
              database_uri='sqlite:///database.sqlite3')

    
trainer = ChatterBotCorpusTrainer(chatty)

trainer.train(
    "chatterbot.corpus.french"
)


youtube_dl.utils.bug_reports_message = lambda: ''


def logsF(text=str):
    logging.info(text)




###############################################bot ready event###############################################
@bot.event
async def on_ready():
    change_status.start()
    print('-------------bot ready-------------')
    print('-----------------------------------')


###############################################Message event###############################################
@bot.event
async def on_message(message):  
    verif = 0
    try:        
        if message.channel.name == 'ludesse':  
            verif = 1
    except:   
        if isinstance(message.channel, discord.channel.DMChannel):
            verif = 1


    if verif == 1:        
        if message.author == bot.user:
            return
        else:
            text = str(message)
            response = chatty.get_response(text)            
            print (f"Ludesse {response}")       
            logsF(response)
            await message.channel.send(response)     
    await bot.process_commands(message)
    # if verif == 1:
    #     before_bad_word=0
    #     bad_word=0
    #     before_db=0
    #     with open("json/db.json") as file:
    #         data=json.load(file)
        
    #     with open ("json/beforeBlacklist.json") as beforeBlacklist:
    #         beforeBlacklistData=json.load(beforeBlacklist)
        
    #     with open ("json/blacklist.json") as blacklist:
    #         blacklistData=json.load(blacklist)
        
    #     with open ("json/beforeDB.json") as bfDB:
    #         beforeDB=json.load(bfDB)
        
    #     # If the message comes from the bot avoid it

    #     for val in beforeBlacklistData:
    #         if val['patterns'].lower() in message.content.lower():
    #             before_bad_word = 1
    #     if before_bad_word == 0:
    #         for value in blacklistData:
    #             if value['patterns'].lower() in message.content.lower():
    #                 rnd_response = random.randint(1, len(value['response']))
    #                 messagel = f"{message.author}: {message.content}"
    #                 print (messagel)
    #                 logsF(messagel)
    #                 key = value['response'][rnd_response -1]
    #                 answer = f"Ludesse: {key}"
    #                 print (answer)       
    #                 logsF(answer)
    #                 await message.channel.send(key) 
    #                 bad_word=1
    #     if bad_word == 0:
    #         for val in beforeDB:
    #             if val['patterns'].lower() in message.content.lower():
    #                 before_db = 1
    #         if before_db == 0:
    #             for keyval in data:
    #                 if keyval['patterns'].lower() in message.content.lower():
    #                     rnd_response = random.randint(1, len(keyval['response']))   
    #                     messagel = f"{message.author}: {message.content}"
    #                     print (messagel)       
    #                     logsF(messagel)
    #                     key = keyval['response'][rnd_response -1]
    #                     answer = f"Ludesse: {key}"
    #                     print (answer)       
    #                     logsF(answer)
    #                     await message.channel.send(key) 


###############################################options for the music###############################################
ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)
        
        self.data = data
        
        self.title = data.get('title')
        self.url = data.get('url')
    
    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
        
        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]
        
        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


###############################################create an invite from a server's id###############################################
@bot.command(name='createInvite', hidden=True)
async def createInvite(ctx, guild_id:int):  
    if ctx.message.author.id == DARKI_ID or ctx.message.author.id == LUCE_ID: 
        guild = bot.get_guild(guild_id)
        for channel in guild.text_channels:
            try:
                invitelink = await channel.create_invite(max_uses=1)
                message = f"Command : createInvite - OK : {guild}"
                await ctx.author.send(invitelink)               
            except Exception:
                message = f"Command : createInvite - EXCEPT : {guild}"
                continue
            else:                
                break
    else:
        await ctx.send(COMMAND_NOT_FOUND)  
        message = f"{ctx.author}: command: createInvite - DENY"
    logsF(message)
    print (message)

###############################################Leave a server from his id###############################################
@bot.command(name='leave', hidden=True)
async def leave(ctx, guild_id:int):  
    if ctx.message.author.id == DARKI_ID or ctx.message.author.id == LUCE_ID: 
        guild = bot.get_guild(guild_id)
        await guild.leave()
        message = f"Command : leaveServer - OK : {guild}"
        await ctx.author.send(message) 
        
    else:
        await ctx.send(COMMAND_NOT_FOUND)  
        message = f"{ctx.author}: command: leaveServer - DENY"
    logsF(message)
    print (message)



###############################################create a loop of 13 seconds###############################################
@tasks.loop(seconds=13)
async def change_status():
    await bot.change_presence(activity=discord.Game(choice(status)))


###############################################General command's error###############################################
@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CommandNotFound):
        await ctx.send(COMMAND_NOT_FOUND)  
        if ctx.guild == None:
            messagel = f"{ctx.author} - COMMAND_NOT_FOUND"
        else:
            messagel = f"{ctx.guild}: {ctx.author} - COMMAND_NOT_FOUND"            
        print (messagel)      
        logsF(messagel)


@bot.command(name='servers', hidden=True)
async def servers(ctx):    
    if ctx.message.author.id == DARKI_ID or ctx.message.author.id == LUCE_ID: 
        print (f"{ctx.author}: command: servers:")
        for server in bot.guilds:              
            message = f"{server.name}"
            logsF(message)
            print (message)
            await ctx.send(f'{server.name} : {server.id}')
        print (f"command: servers: OK")        
        await ctx.author.send("Command: servers: OK") 
    else:
        if ctx.guild == None:
            message = f"{ctx.author}: command: servers - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: servers - DENY"
        await ctx.send(COMMAND_NOT_FOUND)  
        logsF(message)
        print (message)


@bot.command(name='invite', help='Send invite to invite the bot')
async def invite(ctx):
    await ctx.send('link: https://discord.com/api/oauth2/authorize?client_id=799329855761154128&permissions=8&scope=bot')
    if ctx.guild == None:
        message = f"{ctx.author}: command: invite"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: invite"
    print (message)
    logsF(message)


###############################################Log command###############################################
@bot.command(name='log', hidden=True)
async def log(ctx):
    finish = 0
    if ctx.message.author.id == DARKI_ID or ctx.message.author.id == LUCE_ID:
        if ctx.guild == None:
            message = f"{ctx.author}: command: log - OK"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: log - OK"
        print (message)
        logsF(message)
        with open('logs.log') as f:
            line = f.readline()
            while line and finish == 0:
                line = f.readline()
                await ctx.send(line)
    else:
        await ctx.send(COMMAND_NOT_FOUND)
        if ctx.guild == None:
            message = f"{ctx.author}: command: log - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: log - DENY"
        print


###############################################info of the server###############################################
@bot.command(name='info', help='Shows bot information')
async def show_embed(ctx):
    info = (f'guild name: {ctx.guild.name} \n'
        'description:Bot made by <@375564893992124416> \n'
        f'timestamp: {datetime.datetime.utcnow()} \n'
        f'server created at: {ctx.guild.created_at} \n'
        f'server owner: {ctx.guild.owner} \n'
        f'server region: {ctx.guild.region} \n'
        f'server id: {ctx.guild.id}')
    await ctx.send(info)
    if ctx.guild == None:
        message = f"{ctx.author}: command: info"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: info"        
    print (message)
    logsF(message)


###############################################search the internet###############################################
@bot.command(name='search', help='Search an information on internet and return websites')
async def internet(ctx, *, query):
    title = 'Search results'  
    if query.lower() == 'exit':
        await ctx.send("Exit mode search...")
    else:            
        for j in search(query ,tld="com", num=20, stop=7, pause=1):
            embedVar = discord.Embed(title="", description=j, color=0x00ff00)  
            await ctx.send(embed=embedVar)
    if ctx.guild == None:
        message = f"{ctx.author}: command: search: {query}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: search: {query}"         
    print (message)
    logsF(message)  
 
@internet.error
async def internet_error (ctx,error):    
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: search - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: search - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################kick a person from the server###############################################
@bot.command(name='kick', help='Kick a person')
@has_permissions(kick_members=True)
async def kick(ctx, username: discord.Member, *, reason=None):
    await username.kick(reason=reason)    
    await ctx.send(f'The user {username} is kick')
    if ctx.guild == None:
        message = f"{ctx.author}: command: kick: {username}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: kick: {username}"
    print (message)
    logsF(message)

@kick.error
async def kick_error (ctx,error):
    if isinstance(error,MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)      
        if ctx.guild == None:
            message = f"{ctx.author}: command: kick - DENY"  
        else:
            message = f"{ctx.guild}: {ctx.author}: command: kick - DENY" 
        print (message)
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: kick - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: kick - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################check the picture of a person###############################################
@bot.command(name='av', help='Shows the profile picture of a member')
async def av(ctx, *, member:discord.Member = None):
     member = ctx.author if not member else member
     embed = discord.Embed(title = f"{member.name}'s Avatar!", color = member.color , timestamp= ctx.message.created_at)
     embed.set_image(url=member.avatar_url)
     embed.set_footer(text=f"Requested From {ctx.author}") 
     await ctx.send(embed=embed)
     if ctx.guild == None:
        message = f"{ctx.author}: command: av: {member.name}"
     else:
        message = f"{ctx.guild}: {ctx.author}: command: av: {member.name}"
     print (message)
     logsF(message)

@av.error
async def av_error (ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: av - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: av - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################ban a person###############################################
@bot.command(name='ban', help='Ban a person')
@has_permissions(ban_members=True)
async def ban(ctx, username: discord.Member, *, reason=None):
    await username.ban(reason=reason)
    await ctx.send(f'The user {username} is ban')
    if ctx.guild == None:
        message = f"{ctx.author}: command: ban"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: ban: {username}"
    print (message)    
    logsF(message)

@ban.error
async def ban_error (ctx, error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: ban - DENY" 
        else:
            message = f"{ctx.guild}: {ctx.author}: command: ban - DENY" 
        print (message)   
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: ban - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: ban - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################unban a member###############################################
@bot.command(name='unban', help='Unban a member')
@has_permissions(ban_members=True)
async def unban(ctx, *, member):
    banned_users = await ctx.guild.bans()
    member_name,member_discriminator = member.split('#')
    for ban_entry in banned_users:
        user = ban_entry.user
    
        if (user.name, user.discriminator) == (member_name, member_discriminator):
            await ctx.guild.unban(user)
            await ctx.channel.send(f'Unbanned: {user}')
    if ctx.guild == None:
        message = f"{ctx.author}: command: unban"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: unban"
    print (message)   
    logsF(message)
    

@unban.error
async def unban_error(ctx, error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)    
        if ctx.guild == None:
            message = f"{ctx.author}: command: unban - DENY"   
        else:
            message = f"{ctx.guild}: {ctx.author}: command: unban - DENY"   
        print (message)
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING)  
        if ctx.guild == None:
            message = f"{ctx.author}: command: unban - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: unban - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################change user's role###############################################
@bot.command(name='changeRole', help="Change user's role")
@has_permissions(manage_roles=True)
async def changeRole(ctx, username: discord.Member, userRole):
    role = discord.utils.get(ctx.guild.roles, name=userRole)
    if (role in username.roles):    
        await ctx.send(f'The user is already {role}')
    elif (role in ctx.guild.roles):
        await username.add_roles(role)
        await ctx.send(f'The role for the user {username} has changed for {role}')
    else:
        await ctx.send(f"This role doesn't exist")  
    if ctx.guild == None:
        message = f"{ctx.author}: command: changeRole : {username} : {role}"  
    else:
        message = f"{ctx.guild}: {ctx.author}: command: changeRole : {username} : {role}"  
    print (message)  
    logsF(message)  

@changeRole.error
async def changeRole_error(ctx,error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: changeRole - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: changeRole - DENY"
        print (message)
        logsF(message)  
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING)
        if ctx.guild == None:
            message = f"{ctx.author}: command: changeRole - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: changeRole - ARGUMENT_MISSING"
        print (message)
        logsF(message) 


###############################################remove a user's role###############################################
@bot.command(name='removeRole', help="Remove an user's role")
@has_permissions(manage_roles=True)
async def removeRole(ctx, user: discord.Member, userRole):
    guild = ctx.guild
    role = get(guild.roles, name=userRole)
    if (role in user.roles):
        await user.remove_roles(role)
        await ctx.send(f'The user {user} is no longer {role}')
    else:
        await ctx.send(f'The user {user} is not {role}')
    if ctx.guild == None:
        message = f"{ctx.author}: command: removeRole : {role}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: removeRole : {role}"
    print (message)   
    logsF(message) 

@removeRole.error
async def removeRole_error(ctx,error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)
        if ctx.guild == None:
            message = f"{ctx.author}: command: removeRole - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: removeRole - DENY"
        print (message)
        logsF(message) 
    elif isinstance (error, commands.MissingRequiredArgument):
        await ctx.send (ARGUMENT_MISSING)
        if ctx.guild == None:
            message = f"{ctx.author}: command: removeRole - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: removeRole - ARGUMENT_MISSING"
        print (message)
        logsF(message) 


###############################################Delete a role###############################################
@bot.command(name='roleSupression', help='Delete a role')
@has_permissions(manage_roles=True)
async def rero(ctx, *, role: discord.Role):
    if role is None:
        await role.delete()
    
    try:
        await role.delete()
        await ctx.send(f'I **deleted** the role {role}')
    
    except discord.Forbidden:
        await ctx.send('I do not have permission to delete this role')   
    if ctx.guild == None:
        message = f"{ctx.author}: command: roleSupression : {role}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: roleSupression : {role}"
    print (message) 
    logsF(message)    

@rero.error
async def rero_error (ctx,error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)   
        if ctx.guild == None:
            message = f"{ctx.author}: command: roleSupression(rero) - DENY"     
        else:
            message = f"{ctx.guild}: {ctx.author}: command: roleSupression(rero) - DENY"     
        print (message)
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING)
        if ctx.guild == None:
            message = f"{ctx.author}: command: roleSupression(rero) - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: roleSupression(rero) - ARGUMENT_MISSING"
        print (message)
        logsF(message)



@bot.command(name='support', help="Link to go to the support's server")
async def support(ctx):
    if ctx.guild == None:
        message = f"{ctx.author}: command: support"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: support"
    await ctx.send("Link for the support's server : https://discord.gg/XRuxQueZjC")
    print (message)
    logsF(message)



###############################################purge a certain amount of messages###############################################
@bot.command(name='purge', help='Delete a certain amount of messages')
@has_permissions(manage_messages=True)
async def purge(ctx, amount:int):
    await ctx.channel.purge(limit=amount)  
    if ctx.guild == None:
        message = f"{ctx.author}: command: purge: amount {amount}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: purge: amount {amount}"
    print (message)   
    logsF(message)

@purge.error
async def purge_error (ctx, error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)
        if ctx.guild == None:
            message = f"{ctx.author}: command: purge - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: purge - DENY"
        print (message)
        logsF(message)
    elif isinstance (error, commands.MissingRequiredArgument):
        await ctx.send (ARGUMENT_MISSING)
        if ctx.guild == None:
            message = f"{ctx.author}: command: purge - ARGUMENT_MISSING"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: purge - ARGUMENT_MISSING"
        print (message)
        logsF(message)


###############################################count the members on the server###############################################
@bot.command(name='member', help='Shows numbers of members in the server')
async def member_count(ctx):
    a=ctx.guild.member_count
    b=discord.Embed(title=f"members in {ctx.guild.name}", description=a, color=discord.Color((0xffff00)))
    await ctx.send(embed=b)
    if ctx.guild == None:
        message = f"{ctx.author}: command: memberCount"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: memberCount"
    print (message)  
    logsF(message)


###############################################mute a member in put him the role muted###############################################
@bot.command(name='mute', help='Mute a member')
@has_permissions(manage_roles=True)
async def mute(ctx, username: discord.Member):
    if get(ctx.guild.roles, name="Muted"):
        role = discord.utils.get(ctx.guild.roles, name='Muted')
        if "muted" in [y.name.lower() for y in username.roles]:
            await ctx.send(f'The user {username} is already mute')
        else:
            await username.add_roles(role)
            await ctx.send(f'The user {username} is mute')  
        if ctx.guild == None:
            message = f"{ctx.author}: command: mute: {username}"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: mute: {username}"
    else:
        await ctx.send(f"Muted role doesn't exist")
        if ctx.guild == None:
            message = f"{ctx.author}: command: mute: muted not found"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: mute: muted not found"        
    print (message)  
    logsF(message)  

@mute.error
async def mute_error(ctx,error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)
        if ctx.guild == None:
            message = f"{ctx.author}: command: mute - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: mute - DENY"
        print (message)
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING) 
        if ctx.guild == None:
            message = f"{ctx.author}: command: mute - ARGUMENT_MISSING"    
        else:
            message = f"{ctx.guild}: {ctx.author}: command: mute - ARGUMENT_MISSING"    
        print (message)
        logsF(message)


###############################################unmute a member###############################################
@bot.command(name='unmute', help='Unmute a member')
@has_permissions(manage_roles=True)
async def unmute(ctx, username: discord.Member):
    role = discord.utils.get(ctx.guild.roles, name='Muted')
    if "muted" in [y.name.lower() for y in username.roles]:
        await username.remove_roles(role)
        await ctx.send(f'The user {username} is unmute')
    else:
        await ctx.send (f'The user {username} is not mute')
    if ctx.guild == None:
        message = f"{ctx.author}: command: unmute: {username}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: unmute: {username}"
    print (message)
    logsF(message)    

@unmute.error
async def unmute_error(ctx,error):
    if isinstance(error, MissingPermissions):
        await ctx.send(PERMISSIONS_DENIED)  
        if ctx.guild == None:
            message = f"{ctx.author}: command: unmute - DENY"      
        else:
            message = f"{ctx.guild}: {ctx.author}: command: unmute - DENY"      
        print (message)
        logsF(message)
    elif isinstance(error, commands.MissingRequiredArgument):
        await ctx.send(ARGUMENT_MISSING)      
        if ctx.guild == None:
            message = f"{ctx.author}: command: unmute - ARGUMENT_MISSING"  
        else:
            message = f"{ctx.guild}: {ctx.author}: command: unmute - ARGUMENT_MISSING"  
        print (message)
        logsF(message)


###############################################know the latency for the member###############################################
@bot.command(name='ping', help='Shows the latency of the bot')
async def ping(ctx):
    await ctx.send (f"**Pong** Latency: {round(bot.latency * 1000)}ms")
    if ctx.guild == None:
        message = f"{ctx.author}: command: ping"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: ping"
    print (message)  
    logsF(message)


###############################################machine learning###############################################

###############################################Luce join the voice channel###############################################^
@bot.command(name='join', help='Join a voice channel')
async def join(ctx):
    if not ctx.message.author.voice:
        await ctx.send(NOT_IN_A_VOICE_CHANNEL)
        return
    
    else:
        channel = ctx.message.author.voice.channel
    
    await channel.connect()
    if ctx.guild == None:
        message = f"{ctx.author}: command: join"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: join"
    print (message)    
    logsF(message)


###############################################play a song there is on the queue###############################################
@bot.command(name='play', help='Play the song in queue')
async def play(ctx):
    global queue    
    server = ctx.message.guild
    voice_channel = server.voice_client
    
    if not queue:
        await ctx.send(f'The queue is empty')
    else:
        async with ctx.typing():
            player = await YTDLSource.from_url(queue[0], loop=bot.loop)
            voice_channel.play(player, after=lambda e: print('Player error : %s' %e) if e else None)
            del(queue[0])
        
        message = f"**Now playing:** {player.title}"
        await ctx.send(message)
        logMessage = f'{ctx.guild}: {ctx.author}: command: play: {player.title}'
        print (logMessage)    
        logsF(logMessage)


###############################################pause the actual song###############################################
@bot.command(name='pause', help='Pause the actually song')
async def pause(ctx):
    server=ctx.message.guild
    voice_channel = server.voice_client    
    voice_channel.pause()
    await ctx.send(f'**In pause**')
    if ctx.guild == None:
        message = f"{ctx.author}: command: pause"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: pause"
    print (message)  
    logsF(message) 


###############################################resume the actual song###############################################
@bot.command(name='resume', help='Resume a song that is in pause')
async def resume(ctx):
    server=ctx.message.guild
    voice_channel=server.voice_client    
    voice_channel.resume()
    await ctx.send(f'**Resumed**')
    if ctx.guild == None:
        message = f"{ctx.author}: command: resume"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: resume"
    print (message)   
    logsF(message)


###############################################add a song in the queue###############################################
@bot.command(name='queue', help='Put a song in queue')
async def queue_(ctx, url):                 #TODO: ERROR MESSAGE
    global queue
    
    if not ctx.message.author.voice:
        await ctx.send(NOT_IN_A_VOICE_CHANNEL)
    else:
        queue.append(url)
        await ctx.send(f'`{url}` added to queue')
    if ctx.guild == None:
        message = f"{ctx.author}: command: queue:{url}"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: queue:{url}"
    print (message)  
    logsF(message)   


###############################################remove a song from the queue###############################################
@bot.command(name='remove', help='Remove a song from the queue')
async def remove(ctx, number):
    global queue
    
    try:
        del(queue[int(number)])
        if not queue:
            await ctx.send(f'Your queue is now empty')
        else:
            await ctx.send(f'Your queue is now `{queue}`')    
    except:
        await ctx.send('Your queue is either **empty** or the index is **out of range**')
    if ctx.guild == None:
        message = f"{ctx.author}: command: remove"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: remove"
    print (message) 
    logsF(message) 


###############################################show the queue###############################################
@bot.command(name='view', help='Shows the actual queue')
async def view(ctx):
    if not queue:
        await ctx.send(f'Your queue is empty')
    else:
        await ctx.send(f'Your queue is now `{queue}`!')
    if ctx.guild == None:
        message = f"{ctx.author}: command: view"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: view"
    print (message)   
    logsF(message) 

@bot.command(name='stop', help='Stop the music and/or disconnect the voice channel')
async def stop(ctx):
    voice_client = ctx.message.guild.voice_client
    if not voice_client:
        await ctx.send(f'I am not connected to a voice channel')
    else:
        await voice_client.disconnect()
    if ctx.guild == None:
        message = f"{ctx.author}: command: stop"
    else:
        message = f"{ctx.guild}: {ctx.author}: command: stop"
    print (message)  
    logsF(message) 


@bot.command(name='exit', hidden=True)
async def exit(ctx):
    if ctx.message.author.id==DARKI_ID or ctx.message.author.id==LUCE_ID:  
        if ctx.guild == None:
            message = f"{ctx.author}: command: exit - OK"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: exit - OK"
        print (message)  
        logsF(message)
        await ctx.bot.logout()
    else:
        await ctx.send(f"{COMMAND_NOT_FOUND}")  
        if ctx.guild == None:
            message = f"{ctx.author}: command: exit - DENY"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: exit - DENY"
        print (message)  
        logsF(message)


@bot.command(name='broadcast', hidden=True, pass_context=True)
async def broadcast(ctx, *, msg):
    if ctx.message.author.id==DARKI_ID or ctx.message.author.id==LUCE_ID:
        if ctx.guild == None:
            message = f"{ctx.author}: command: broadcast-{msg} - OK"
        else:
            message = f"{ctx.guild}: {ctx.author}: command: broadcast-{msg} - OK"
        print (message)
        logsF(message)
        for server in bot.guilds:
            for channel in server.text_channels:
                if channel.name == 'ludesse':
                    await channel.send(msg)
                    print (f"----OK {server}-{channel}: {msg}")
                else:
                    print (f"----BREAK {server}-{channel}: {msg}")
        await ctx.send(f"Broadcast OK")
    else:
        await ctx.send(f"{COMMAND_NOT_FOUND}")   
        if ctx.guild == None:
            message = f"{ctx.author}: command: broadcast-{msg} - DENY"   
        else:
            message = f"{ctx.guild}: {ctx.author}: command: broadcast-{msg} - DENY"   
        print (message)
        logsF(message)

bot.run('Nzk5MzI5ODU1NzYxMTU0MTI4.YAB_8A.EcLE8B8S79aq_e1g-SZduVVeM4w')